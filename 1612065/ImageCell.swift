//
//  TableViewCell.swift
//  FreePic
//
//  Created by Le Nguyen Hoang Cuong on 11/21/18.
//  Copyright © 2018 spiritofthecore. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var mainImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
        }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(mainImageView)
        
        mainImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        mainImageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        mainImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        mainImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func downloadImage(url: URL) {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = self.mainImageView.bounds
        self.mainImageView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        DispatchQueue.global().async {
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async { () -> Void in
                guard let data = data, error == nil else { return }
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                self.mainImageView.image = UIImage(data: data)
            }
            }
            task.resume()
        }
    }
}
