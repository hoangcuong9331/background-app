//
//  Picture.swift
//  1612065
//
//  Created by Le Nguyen Hoang Cuong on 11/24/18.
//  Copyright © 2018 spiritofthecore. All rights reserved.
//

import UIKit

class Picture {
    var imageWidth: CGFloat!
    var imageHeight: CGFloat!
    var largeImageURL: URL!
    var previewURL: URL!
    init(imageWidth: CGFloat!, imageHeight: CGFloat!, largeImageURL: URL!,  previewURL: URL!, user: String!, views: Int!, likes: Int!, download: Int!) {
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.largeImageURL = largeImageURL
        self.previewURL = previewURL
    }
    init() {
        self.imageWidth = nil
        self.imageHeight = nil
        self.largeImageURL = nil
        self.previewURL = nil
    }
}
