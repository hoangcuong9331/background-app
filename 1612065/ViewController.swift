//
//  ViewController.swift
//  1612065
//
//  Created by Le Nguyen Hoang Cuong on 11/22/18.
//  Copyright © 2018 spiritofthecore. All rights reserved.
//

import UIKit

var images = [Picture]()

class TableViewController: UITableViewController, UISearchBarDelegate {

    var currentPage: Int = 1
    var searchContent: String = "all"
    @IBAction func NextPage(_ sender: Any) {
        currentPage = currentPage + 1
        newPictures(currentPage: self.currentPage, searchText: searchContent)
    }
    @IBAction func HomePage(_ sender: UIBarButtonItem) {
        currentPage = 1
        searchContent = "all"
        newPictures(currentPage: self.currentPage, searchText: searchContent)
    }
    @IBOutlet weak var SearchPicture: UISearchBar!
    var selectedRow: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        SearchPicture.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.register(ImageCell.self, forCellReuseIdentifier: "ImageCell")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        newPictures(currentPage: 1, searchText: "all")
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SearchPicture
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell") as! ImageCell
        cell.downloadImage(url: images[indexPath.row].largeImageURL)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let imageCrop = images[indexPath.row].imageWidth/images[indexPath.row].imageHeight
        return tableView.frame.width / imageCrop
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        performSegue(withIdentifier: "ShowDetails", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ShowDetails"){
            let detailController:ImageDetailsController = segue.destination as! ImageDetailsController
            detailController.indexPicture = selectedRow
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard searchBar.text != "" else {
            newPictures(currentPage: self.currentPage, searchText: "all")
            return
        }
        currentPage = 1
        searchContent = searchBar.text!
        newPictures(currentPage: currentPage, searchText: searchContent)
    }

    func newPictures (currentPage: Int, searchText: String){
        images.removeAll()
        var url: URL!
        if searchText == "all"
        {
            url = URL(string: "https://pixabay.com/api/?key=10792494-5619a603f22c5faa7fe9decb7&page=\(currentPage)&image_type=photo")
        } else {
            url = URL(string: "https://pixabay.com/api/?key=10792494-5619a603f22c5faa7fe9decb7&page=\(currentPage)&q=\(searchText)&image_type=photo")
        }
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
            if error != nil {
                print("\(String(describing: error))")
            } else {
                do {
                let parsedData = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                
                for(key, value) in parsedData {
                    if key == "hits" {
                        if let hitsArray: [ [String: Any] ] = value as? [ [String: Any] ] {
                            for hit in hitsArray {
                                let Pic: Picture = Picture()
                                for (key, value) in hit {
                                    if (key == "imageWidth") {
                                        let iW = value as? Int
                                        Pic.imageWidth = CGFloat(iW!)
                                    }
                                    if key == "imageHeight" {
                                        let iH = value as? Int
                                        Pic.imageHeight = CGFloat(iH!)
                                    }
                                    if key == "largeImageURL" {
                                        let lIU = URL(string: (value as? String)!)
                                        Pic.largeImageURL = lIU
                                    }
                                    if key == "previewURL" {
                                        let pU = URL(string: (value as? String)!)
                                        Pic.previewURL = pU
                                    }
                                }
                                images.append(Pic)
                            }
                        }
                    }
                    }
                } catch {
                    print("failed to load data!")
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
        task.resume()
    }
}


