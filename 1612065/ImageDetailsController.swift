//
//  ImageDetailsController.swift
//  FreePic
//
//  Created by Le Nguyen Hoang Cuong on 11/22/18.
//  Copyright © 2018 spiritofthecore. All rights reserved.
//

import UIKit

class ImageDetailsController: UIViewController {

    var indexPicture: Int!
    //Action
    @IBAction func SetWallPaper(_ sender: Any) {
        URLSession.shared.dataTask(with: images[indexPicture].largeImageURL) {(data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            DispatchQueue.main.async {
                UIImageWriteToSavedPhotosAlbum(UIImage(data: data!)!, nil, nil, nil)
                let succeed = UIAlertController(title: "Download", message: "Download succeed", preferredStyle: .alert)
                self.present(succeed, animated: true, completion: nil)
            }
            }.resume()
    }
    @IBAction func FullXScr(_ sender: Any) {
        
    }
    @IBAction func Share(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: [self.ImageContainer.image!], applicationActivities: nil)
    activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    //Outlet
    @IBOutlet weak var ImageContainer: UIImageView!
    
    //ToDo
    override func viewDidLoad() {
        super.viewDidLoad()
        URLSession.shared.dataTask(with: images[indexPicture].largeImageURL) {(data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            DispatchQueue.main.async {
                self.ImageContainer.image = UIImage(data: data!)!
            }
            }.resume()
    }
}
